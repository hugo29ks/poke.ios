//
//  ImagenViewController.swift
//  pokemon
//
//  Created by Hugo Montero on 20/6/18.
//  Copyright © 2018 Hugo Montero. All rights reserved.
//

import UIKit

class ImagenViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weight: UILabel!
   
    var itemInfoPokemon:(itemManagerPokemon: pokemon,index: Int)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].name
        let weightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].weight)!
        let heightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].height)!
        weight.text = String(weightTemp)
        heightLabel.text = String(heightTemp)
        
        let bm = Network()
        bm.getImage(url: "", (itemInfoPokemon?.index)!, completionHandler: { (imageR) in
            DispatchQueue.main.async {
                self.pokemonImageView.image = imageR
            }
        })
    }

    
}
